//Save and (un)equip a set of inventory
//Click to enable/disable
//Hold Control to "set" your set
var actor = canvas.tokens.controlled[0]?.actor ?? actor;
if (actor) {
	if (game.keyboard.isDown('Control')) {
		actor.setFlag('world', 'equipSet', actor.items.filter(o => o.data.data.equipped).map(p => p.id));
		ui.notifications.info('Equipment saved for ' + actor.name);
	}
	else {
		let items = actor.getFlag('world', 'equipSet');
		if (items) {
			let onOff = !actor.items.find(o => o.data.data.equipped);
			items = items.flatMap(o => {
				if (actor.items.get(o)) return {_id: o, 'data.equipped': onOff};
				return [];
			});
			actor.updateEmbeddedEntity('OwnedItem', items);
			ui.notifications.info(`Equipment set to ${onOff ? 'Equipped' : 'Unequipped'}`);
		}
		else
			ui.notifications.warn('No equipment set "set". Control + click the macro.');
	}
}
else
	ui.notifications.error('No actor selected');
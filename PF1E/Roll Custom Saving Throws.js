// CONFIGURATION
// Leave the actorNames array empty to guess the players
// Example actorNames: `actorNames: ["Bob", "John"],`
// Set customFormula to what you want
// Example customFormula: '2d20r1kh'
const c = {
  actorNames: [],
  customFormula: '1d20'
};
// END CONFIGURATION

const tokens = canvas.tokens.controlled;
let actors = tokens.map(o => o.actor);
if (!actors.length && c.actorNames.length) actors = game.actors.entities.filter(o => c.actorNames.includes(o.name));
if (!actors.length) actors = game.actors.entities.filter(o => o.isPC && o.hasPerm(game.user, "OWNER"));
actors = actors.filter(o => o.hasPerm(game.user, "OWNER"));

if (!actors.length) ui.notifications.warn("No applicable actor(s) found");
else {
  const _roll = async function(type) {
    for (let o of actors) {
      await rollSavingThrow.call(o, type, { event: new MouseEvent({}) });
    }
  };

  const msg = `Choose a saving throw to roll for the following actor(s): <strong>${actors.map(o => o.name).join("</strong>, <strong>")}</strong>`;

  new Dialog({
    title: "Roll saving throw",
    content: `<p>${msg}</p>`,
    buttons: {
      fort: {
        label: "Fortitude",
        callback: () => _roll("fort"),
      },
      ref: {
        label: "Reflex",
        callback: () => _roll("ref"),
      },
      will: {
        label: "Will",
        callback: () => _roll("will"),
      },
    },
  }).render(true);
}

var rollSavingThrow = async function(savingThrowId, options={ event: null, noSound: false, skipPrompt: true }) {
// Add contextual notes
let notes = [];
let rollData = this.getRollData();
const noteObjects = this.getContextNotes(`savingThrow.${savingThrowId}`);
for (let noteObj of noteObjects) {
  rollData.item = {};
  if (noteObj.item != null) rollData = noteObj.item.getRollData();

  for (let note of noteObj.notes)
	notes.push(...note.split(/[\n\r]+/).map(o => TextEditor.enrichHTML(o, {rollData: rollData})));
}

// Roll saving throw
let props = this.getDefenseHeaders();
if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });
const label = CONFIG.PF1.savingThrows[savingThrowId];
const savingThrow = this.data.data.attributes.savingThrows[savingThrowId];
var customRoll = (new Roll(c.customFormula)).roll();
var customTooltip = await customRoll.getTooltip();
return game.pf1.DicePF.d20Roll({
  event: options.event,
  parts: ["@mod"],
  situational: true,
  staticRoll: customRoll.total,
  data: { mod: savingThrow.total },
  title: game.i18n.localize("PF1.SavingThrowRoll").format(label),
  speaker: ChatMessage.getSpeaker({actor: this}),
  takeTwenty: false,
  fastForward: options.skipPrompt !== false ? true : false,
  chatTemplate: "systems/pf1/templates/chat/roll-ext.html",
  chatTemplateData: { hasProperties: props.length > 0, properties: props, formula: customRoll.formula + ' + ' + savingThrow.total, tooltip: customTooltip },
  noSound: options.noSound,
});
};
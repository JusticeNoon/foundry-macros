/*----------Swap to PC/NPC-----------*/
/*------ "character" or "npc" -------*/
var destinationType = "character";

canvas.tokens.controlled.forEach(t => {
	if (t && t.actor) {
		const actData = t.actor.toObject();
		delete actData._id;
		delete actData.flags.core?.sheetClass;
		actData.type = destinationType;
		actData.name += ` (${destinationType})`;
		Actor.create(actData);
	}
});
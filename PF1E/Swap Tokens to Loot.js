// Changes selected tokens that are defeated to loot sheets. Supports unlinked tokens without having to copy them.
// Sheetnames are "PF1.LootSheetPf1NPC" for loot sheet module and "PF1.ActorSheetPFNPCLoot" for vanilla loot sheet
const popupAfterDone = false,
	sheetName = "pf1.ActorSheetPFNPCLoot";

let userPerm = {default: 0},
	allTokens = canvas.tokens.controlled;
	
game.users.forEach(o => {
	if (o.character) userPerm[o.id] = 2;
});
var updateTok = async function(singleToken) {
	const actor = singleToken.actor;
	const original = actor.getFlag("core", "sheetClass");
	
	const sheet = actor.sheet;
	await sheet.close();
	actor._sheet = null;
	delete actor.apps[sheet.appId];

	await actor.update({"permission": userPerm, "flags.core.sheetClass": sheetName});
	actor.sheet.render(popupAfterDone);
};
allTokens.forEach((tok) => updateTok(tok));
// CONFIGURATION
// actorNames overrides selected tokens.
// Leave empty to use currently selected tokens/ fill to always use the same actorNames
// Example actorNames: `actorNames: ["Bob", "John"],`
// The rest are localization strings.
// Conditions should apply to the current localization but these config strings need to be done manually.
const c = {
	actorNames: [],
	windowTitle: "Condition Selection",
	windowSubtitle: "Apply conditions to the following tokens: ",
	buttonLabels: ["Add", "Remove", "Apply", "Clear", "Toggle"],
	warningMissingActors: "No applicable actor(s) found"
};
// END CONFIGURATION

//Set up targets and verify they can be edited
const myTokens = canvas.tokens.controlled;
let myActors,
	preselected;
if (c.actorNames.length > 0)
	myActors = game.actors.filter(o => c.actorNames.includes(o.name));
else
	myActors = myTokens.map(o => o.actor);
myActors = myActors.filter(o => o.isOwner);
if (typeof state !== "undefined") { 
	if (typeof token !== "undefined") myActors = [token.actor];
	else if (typeof actor !== "undefined") myActors = [actor];
}
if (window.macroChain?.length || event.target.nodeName == 'A') {
	let inputText = window.macroChain?.pop() || event.target.textContent.trim(),
	regex = new RegExp(`(${c.buttonLabels.join('|')})(?:(?<!${c.buttonLabels[3]}) +([^?]+))?(\\?)?`),
	[button, conditions, prompt] = inputText.trim().match(regex)?.slice(1) ?? [];
	if (button) {
		button = c.buttonLabels.findIndex(o => o.toLowerCase().localeCompare(button.trim().toLowerCase(), undefined, {ignorePunctuation: true}) === 0);
		let lcConditions = Object.entries(CONFIG.PF1.conditions).map(o => [o[0], o[1].toLowerCase()]);
		conditions = conditions?.split(/\s*,\s*/).map(o => o.toLowerCase())
		.flatMap(conSelect => {
			var found = lcConditions.find(p => conSelect == p[0] || conSelect.localeCompare(p[1], undefined, {ignorePunctuation: true}) === 0);
			if (found)
				return ['data.attributes.conditions.' + found[0]];
			else
				return [];
		}) ?? [];
		preselected = {button, conditions, prompt: prompt || game.keyboard.isModifierActive("Shift")};
	}
}

//Main codeblock
if (!myActors.length) ui.notifications.warn(c.warningMissingActors);
else {
	let _addCond = function(htm,cond) {
		let checks;
		if (cond) checks = cond.map(o => [o, true]);
		else
			checks = Array.from(htm.querySelectorAll('input:checked')).map(o => [o.name, true]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	};
	
	let _remCond = function(htm,cond) {
		let checks;
		if (cond) checks = cond.map(o => [o, false]);
		else
			checks = Array.from(htm.querySelectorAll('input:checked')).map(o => [o.name, false]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	};
	
	let _applyCond = function(htm,cond) {
		let checks;
		if (cond)
			checks = Object.keys(CONFIG.PF1.conditions).map(o => ['data.attributes.conditions.' + o, cond.includes('data.attributes.conditions.' + o)]);
		else
			checks = Array.from(htm.querySelectorAll('input')).map(o => [o.name, o.checked]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	};
	
	let _clearCond = function(htm,cond) {
		let checks;
		if (cond)
			checks = Object.keys(CONFIG.PF1.conditions).map(o => ['data.attributes.conditions.' + o, false]);
		else
			checks = Array.from(htm.querySelectorAll('input')).map(o => [o.name, false]);
		myActors.forEach(o => {
			o.update(Object.fromEntries(checks));
		});
	};
	
	let _toggleCond = function(htm,cond) {
		myActors.forEach(o => {
			var checks = [];
			cond.forEach(con => {
				if (getProperty(o.data, con)) checks.push([con, false]);
				else checks.push([con, true]);
			});
			o.update(Object.fromEntries(checks));
		});
	};
	
	var msg = '<p>' + c.windowSubtitle + myActors.map(o => o.name).join(', ') + '</p><div class="flexrow">';
		
	Object.keys(CONFIG.PF1.conditions).forEach(o => {
		msg += `
<div style="flex: auto; padding: 0 .5em;">
	<label class="checkbox">
	<input type="checkbox" name="data.attributes.conditions.${o}">
	<span>${CONFIG.PF1.conditions[o]}</span>
	<label>
</div>`;
	});
	msg += '</div>';
	
	var buttons = {
		add: {
			label: c.buttonLabels[0],
			callback: html => _addCond(html[0])
		},
		remove: {
			label: c.buttonLabels[1],
			callback: html => _remCond(html[0])
		},
		apply: {
			label: c.buttonLabels[2],
			callback: html => _applyCond(html[0])
		},
		clear: {
			label: c.buttonLabels[3],
			callback: html => _clearCond(html[0])
		}
	};
	
	const dialog = new Dialog({
		title: game.i18n.localize('PF1.ConditionPlural'),
		content: `<p>${msg}</p>`,
		buttons,
		default: Object.keys(buttons)[preselected?.button] ?? 'add',
		render: (html) => {
			[...html.find("label.checkbox input")].forEach(inp => {
				var affected = myActors.filter(act => getProperty(act.data, inp.name));
				if (affected.length > 0) {
					inp.indeterminate = true;
					inp.parentNode.title = affected.map(act => act.name);
				}
				if (preselected?.prompt && preselected.conditions?.includes(inp.name))
					inp.checked = true;
					inp.indeterminate = false;
			});
		}
	});	

	if (!preselected || preselected.prompt)
		dialog.render(true);
	else {
		let buttonActions = [_addCond, _remCond, _applyCond, _clearCond, _toggleCond];
		buttonActions[preselected.button](null, preselected.conditions);
	}
	
}
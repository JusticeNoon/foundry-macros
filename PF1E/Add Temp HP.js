//Add or subtract Temp HP. Will do math or rolls.
//Preface with "=" to set to exactly that number
/* Alt Use: */
//Can be used wtihin entity link buttons. Anything but the first number is ignored.
//"->" can be used to subtract the number of the next inline roll encountered
//"+>" can be used to add the number of the next inline roll encountered
//"=>" can be used to set to the number of the next inline roll encountered
//Examples: (All are equivalent)
//	@Macro[Add Temp HP]{Add Temp HP ->}[[5+3]]
//	@Macro[Add Temp HP]{+8 THP}
//	@Macro[Add Temp HP]{Set to =>}[[@attributes.hp.temp + 3 + 5]]

var controlled = canvas.tokens.controlled.map(o => o.actor),
	inputText = false,
	chatMessage = game.messages.get(event.target.closest('.message')?.getAttribute('data-message-id'));
if (window.macroChain?.length || event.target.nodeName == 'A')
	inputText = window.macroChain?.pop() || event.target.textContent.trim();

if (inputText) {
	if (inputText.indexOf('->') > -1)
		inputText = '-1 * (' + event.target.nextElementSibling?.textContent.trim() + ')';
	else if (inputText.indexOf('+>') > -1)
		inputText = event.target.nextElementSibling?.textContent.trim();
	else if (inputText.indexOf('=>') > -1)
		inputText = '=' + event.target.nextElementSibling?.textContent.trim();
	else
		inputText = inputText.match(/([=-]?\d+)/)?.[1];
}
if (!inputText) {
	let d = new Dialog({
		title: 'How much Temp HP?',
		content: '<input name="temphp" type="text" class="temphp select-on-click" placeholder="+Temp." title="Temporary Hit Points" autofocus>',
		buttons: {
		  ok: {
			icon: '',
			label: "OK",
			callback: htm => applyTHP(htm)
		  }
		},
		default: 'ok'
	  }, {width: 100});
	Hooks.once('renderDialog', (a,inp) => setTimeout(() => inp[0].querySelector('input').focus(), 150));
	d.render(true);
}
else
	applyTHP(null, inputText);

function applyTHP(htm, fromButton=false) {
	controlled.forEach(selected => {
		let change = 0, equal;
		if (fromButton || fromButton === '0')
			change = fromButton;
		else
			change = htm[0].querySelector('input').value;
		if (change.charAt(0) == '=') {
			equal = true;
			change = change.slice(1);
		}
		let parsed = parseInt(RollPF.safeTotal(change)),
			difference = Math.max(selected.data.data.attributes.hp.temp + parsed, 0);
		if (equal)
			selected.update({'data.attributes.hp.temp': parsed});
		else
			selected.update({'data.attributes.hp.temp': difference});
	});
}
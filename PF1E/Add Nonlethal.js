//Add or subtract Nonlethal damage. Will do math or rolls.
//Preface with "=" to set to exactly that number
/* Alt Use: */
//Can be used wtihin entity link buttons. Anything but the first number is ignored.
//"->" can be used to subtract the number of the next inline roll encountered
//"+>" can be used to add the number of the next inline roll encountered
//"=>" can be used to set to the number of the next inline roll encountered
//Examples: (All are equivalent)
//	@Macro[Add Nonlethal]{Apply Damage ->}[[5+3]]
//	@Macro[Add Nonlethal]{+8 NLD}
//	@Macro[Add Nonlethal]{Set nonledam to =>}[[@attributes.hp.nonlethal + 3 + 5]]

var controlled = canvas.tokens.controlled.map(o => o.actor),
	inputText = false,
	chatMessage = game.messages.get(event.target.closest('.message')?.getAttribute('data-message-id'));
if (window.macroChain?.length || event.target.nodeName == 'A')
	inputText = window.macroChain?.pop() || event.target.textContent.trim();

if (inputText) {
	if (inputText.indexOf('->') > -1)
		inputText = '-1 * (' + event.target.nextElementSibling?.textContent.trim() + ')';
	else if (inputText.indexOf('+>') > -1)
		inputText = event.target.nextElementSibling?.textContent.trim();
	else if (inputText.indexOf('=>') > -1)
		inputText = '=' + event.target.nextElementSibling?.textContent.trim();
	else
		inputText = inputText.match(/([=-]?\d+)/)?.[1];
}
if (!inputText) {
	let d = new Dialog({
		title: 'How much Nonlethal Damage?',
		content: '<input name="nonlethal" type="text" class="temphp select-on-click" placeholder="+Nonlethal" title="Nonlethal" autofocus>',
		buttons: {
		  ok: {
			icon: '',
			label: "OK",
			callback: htm => applyNLD(htm)
		  }
		},
		default: 'ok'
	  }, {width: 100});
	Hooks.once('renderDialog', (a,inp) => setTimeout(() => inp[0].querySelector('input').focus(), 150));
	d.render(true);
}
else
	applyNLD(null, inputText);

function applyNLD(htm, fromButton=false) {
	controlled.forEach(selected => {
		let change = 0, equal;
		if (fromButton || fromButton === '0')
			change = fromButton;
		else
			change = htm[0].querySelector('input').value;
		if (change.charAt(0) == '=') {
			equal = true;
			change = change.slice(1);
		}
		let parsed = parseInt(RollPF.safeTotal(change)),
			difference = Math.max(selected.data.data.attributes.hp.nonlethal + parsed, 0);
		if (equal)
			selected.update({'data.attributes.hp.nonlethal': parsed});
		else
			selected.update({'data.attributes.hp.nonlethal': difference});
	});
}
//Change extraHealth to whatever you want. Math is fine. It will be applied per character. 
//ie Adding con mod but not subtracting it would be
// extraHeal = "max(0, @abilities.con.mod)"
var extraHeal = "0";

//Calls rest on selected tokens OR all PCs if none selected
let chars = (canvas.tokens.controlled.length > 0 ? canvas.tokens.controlled.map(o => o.actor) : game.actors).filter(o => o.data.type == 'character' && o.isOwner);
chars.forEach(ch => {
	let heal = RollPF.safeTotal(extraHeal, ch.data.data);
	if (heal != 0)
		ch.update({"data.attributes.hp.value": Math.min(ch.data.data.attributes.hp.value + heal, ch.data.data.attributes.hp.max)}, {updateChanges: false});
	ch.sheet._onRest(new Event('ImSoTired'));
});
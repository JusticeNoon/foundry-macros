var tokens = canvas.tokens.controlled;
switch(tokens.length) {
	case 0:
		ui.notifications.warn('No tokens selected');
		break;
	case 1:
		if (!tokens[0].actor) { ui.notifications.warn('No actor for this token'); break; }
		var classes = Object.entries(tokens[0].actor.getRollData().attributes.spells.spellbooks)
			.filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[0], p[1].class]);
		if (classes.length == 0) ui.notifications.warn('No caster classes');
		else if (classes.length == 1) tokens[0].actor.rollConcentration(classes[0][0]);
		else {
			var buttons = classes.map(o => {
				return {label: o[1], callback: () => tokens[0].actor.rollConcentration(o[0]) };
			});
			new Dialog({
				title: 'Roll Concentration',
				content: 'Which class to roll for?',
				buttons: buttons
			}).render(true);
		}
		break;
	default:
		tokens.forEach(o => o.actor.rollConcentration('primary'));	
}
//Put actor names you want to check here
var charName = ["Strong Guy", "Magic Dude"]

function calcActor(charName) {
	var charObj = game.actors.getName(charName);
	if (!charObj?.isOwner) return;
	var coinVal = Object.values(charObj.data.data.currency).reduce((p,q,r) => p + q * (10 / Math.pow(10,r)), 0),
	altCoin = Object.values(charObj.data.data.altCurrency).reduce((p,q,r) => p + q * (10 / Math.pow(10,r)), 0);
	
	var msgText = `<b>Items:</b> ${calculateSellItemValue(charObj)} gp<br>
	<b>Currency:</b> ${Math.round(coinVal * 100) / 100} gp<br>
	<b>Alt-Currency:</b> ${Math.round(altCoin * 100) / 100} gp`;
	ChatMessage.create({
		flavor: charName + "'s value:",
		speaker: ChatMessage.getSpeaker(),
		content:     msgText,
		type: CONST.CHAT_MESSAGE_TYPES.OTHER,
		whisper: ChatMessage.getWhisperRecipients(game.user.name)
	});
}

function calculateSellItemValue(actorObj) {
    const items = actorObj.items;
    return Math.round(items.reduce((cur, i) => {
        if (i.data.type === "loot" && i.data.data.subType === "tradeGoods") return cur + (i.data.data.price * i.data.data.quantity);
        if (i.data.data.price) return cur + (i.data.data.price * i.data.data.quantity);
        return cur;
    }, 0) * 100) / 100;
}

if (typeof charName == "object") charName.forEach(calcActor);
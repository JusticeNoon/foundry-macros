/*-			DOCUMENTATION			-*/
// Syntax: [<Select|ActorOverride|Open:] [Roll] <Item Name>[;<Action Name|Action Number>] [#<type>]
// ie @Macro[useAction]{Manyshot #attack}
// Item Name: 
//		All items on the character sheet will be searched.
//		The first action one to be found will be used.
//		End with a "?" to get a prompt of available actions
//		Put parts in double quotes to mark them as required for fuzzy searches
//		Add ; or > to specify action name or number (eg, Fireball;Cast)
// Type (Optional, Prefixed by "#"):
//		If not specified, it will not care and just return the first actionable action.
//		Capitilization does not matter here
//		For spells, you can include a level to further filter. Acceptable formats are "3-5","3/4/5", "3+", "3-", "3"
// ActorOverride (Optional, Suffixed by ":"):
//		Sets the remaining actions to use the overriden actor by name
//		If not specified, will use the default target actor priority when searching (speaker, character, selected)
//		This is case sensitive. You can reset back to default by providing the keyword "My:"
//		ie @Macro[useAction]{His: Manyshot, AmmoCount, My: OP Dodge Ability}
// Select:
//		Treats the remaining actions as a list of things to do
// Open:
//		Opens item sheet. Can be forced by holding "Alt"
// Roll
//      Rolls something rollable for an actor. Uses i18n translations.
// You may separate your actions by comma to execute multiple ie @Macro[useAction]{Magic Missile, Magic Missile, Crossbow}
// Commas can be used in names by escaping them with \, or ,,
// If it can't find an exact match for item, it will use the first actionable item containing the name
// Holding "Control" will use an item's roll instead of the action
// Supports label mode. @Macro[useAction]{-El Diablo-, +5 Greatsword of Exploding::Time to hit something}

/*-			CONFIGURATION			-*/
const c = {
	commands: {
		select: "Select",
		open: "Open",
		roll: "Roll",
		my: "my",
	},
	labels: {
		hideChargeless: "Hide actions without enough charges",
	}
}

/*-			COMMAND					-*/
try {
	if (typeof event === "undefined") event = new Event("none");
	if (event instanceof MessageEvent) var socketData = event.data ? JSON.parse(event.data.replace(/[^{([]+/,""))?.[0].result[0] : null;
	var inputText = scope?.command?.replace(/_/g," ") || window.macroChain?.pop() || (typeof args == "object" ? args.join(" ") : false) || event.target?.closest?.('button,a')?.textContent.trim(),
		chatMessage = socketData ? game.messages.get(socketData._id) : game.messages.get(event.target?.closest?.('.message')?.getAttribute('data-message-id')),
		targetActor = event.args?.[1] ?? ChatMessage.getSpeakerActor(chatMessage?.speaker) ?? actor ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		targetType = 'all',
		thisMacro = this.name,
		argParse = /^(?:([^:]*):\s*)?(.*?)(?:\s*(?:;|-?>)\s*(.*?))?(?:\s*?#\s*(.*))?$/,
		actionList,
		allTypes = Object.fromEntries(game.system.documentTypes.Item.map(o => [o, game.i18n.localize('TYPES.Item.' + o)]));
	if (event.target?.closest?.('button,a')?.dataset.extra)
		inputText = event.target.closest('button,a').dataset.extra;
	actionList = inputText.split(/(?<![,\\]),(?!,)/).map(o => o.trim().replace(/[,\\],/g, ","));
	if (!actionList || !actionList.length) throw ui.notifications.error("No action specified.");
}
catch (err) {
	ui.notifications.error("Command failed. Something went wrong.");
	console.error("Whatever you did didn't work", err, inputText);
}

actionList.some((act, actIdx) => {
	var [actorOverride, itemName, actionName, itemType] = act.match(argParse).slice(1),
		doPrompt, dirtySearch, typeFilter, openItem;
	if (itemName.indexOf('?') > -1) {
		doPrompt = true;
		itemName = itemName.replace('?','');
	}
	if (itemName.indexOf('"') > -1) {
		itemName = itemName.replace(/"([^"]*)"/g, (full,part) => {
			dirtySearch = dirtySearch?.concat(part) ?? [part];
			return part;
		});
	}
	if (itemType) {
		var [empty, range, pivot, specific] = itemType.match(/\s*(?:(\d\s*-\s*\d)|(\d[-+])|(\d(?:\s*&.+)?))\s*/) ?? [];
		if (range) typeFilter = [...Array(10).keys()].slice(...range.match(/(\d)\s*-\s*(\d)/).slice(1).map((o,idx) => parseInt(o)+idx));
		else if (pivot) typeFilter = [...Array(10).keys()].slice(...pivot.replace(/(\d)(-)?\+?/, (m,p1,p2) => (p2 ? '0,' : '') + p1).split(',').map((o,idx) => parseInt(o)+idx));
		else if (specific) typeFilter = specific.split(/\s*&\s*/).map(o => parseInt(o));
		if (empty) itemType = itemType.replace(empty, '');
		itemType = Object.keys(allTypes).find(o => allTypes[o].localeCompare(itemType, game.i18n.lang, {sensitivity: 'base'}) === 0);
	}
	if (actorOverride) {
		if (actorOverride.toLowerCase() == c.commands.my.toLocaleLowerCase())
			targetActor = game.actors.get(chatMessage?.speaker?.actor) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor;
		else if (actorOverride.localeCompare(c.commands.select, game.i18n.lang, {sensitivity: 'base'}) === 0)
			return renderSelect([act.replace(actorOverride,'')].concat(actionList.slice(actIdx + 1)));
		else if (actorOverride.charAt(0) == '_') {
			let actId, sceneId, tokId;
			actId = actorOverride.match(/_Actor\.(.*)/)?.[1];
			[sceneId, tokId] = actorOverride.match(/_Scene\.([^.]*).Token\.(.*)/)?.slice(1) ?? [null, null];
			targetActor = game.actors.get(actId) ?? targetActor;
			if (canvas.scene.id == sceneId)
				targetActor = canvas.tokens.get('tokId').actor ?? targetActor;
		}
		else if (actorOverride.toLocaleLowerCase() == c.commands.roll.toLocaleLowerCase())
			itemName = c.commands.roll + " " + itemName;
		else if (actorOverride.toLocaleLowerCase() == c.commands.open.toLocaleLowerCase())
			openItem = true;
		else targetActor = game.actors.getName(actorOverride);
	}
	if (targetActor) {
		if (!targetActor.isOwner) return ui.notifications.warn(game.i18n.localize("PF1.Error.NoActorPermission"));
		var backupAction = false,
			item = false,
			actionPrompts = [];
		if ((new RegExp(c.commands.roll,'i')).test(itemName)) {
			rollAction(itemName, targetActor);
			return false;
		}
		targetActor.items.forEach(o => {
			var lcItName = o.name.toLocaleLowerCase();
			if (itemType && itemType != o.type) return false;
            if (!backupAction && lcItName.indexOf(itemName.toLocaleLowerCase()) > -1)
				backupAction = o;
			if (o.type != 'spell' && !o.isCharged && !o.hasAction) return false;
			if (dirtySearch?.every(p => lcItName.indexOf(p.toLocaleLowerCase()) > -1))
				actionPrompts.push(o);
			if (o.name == itemName)
				item = o;
		});
		if (!item) {
			if (backupAction)
				item = backupAction;
			else if (actionPrompts.length > 0)
				item = actionPrompts[0];
			else if (itemName != '' && !dirtySearch)
				ui.notifications.warn(game.i18n.format('PF1.Warning.NoItemOnActor', [`"${targetActor.name}"`, `"${itemName}"`]));
		}
		if (doPrompt) {
			targetType = item?.type ?? targetType;
			if (actionPrompts.length > 0)
				return renderSelect(actionPrompts, item?.name, actionList.slice(actIdx + 1));
			return renderSelect(targetActor.items.filter(o => {
				if (targetType == 'all') return true;
				if (o.type == targetType && 
					(!typeFilter || (typeFilter?.includes(parseInt(o.system.level))))) return true;
				return false;
			}), item?.name, actionList.slice(actIdx + 1));
		}
		else {
			if (!item)
				return ui.notifications.error(game.i18n.format('PF1.Warning.NoItemOnActor', [`"${targetActor.name}"`, `"${itemName}"`]));
			if (game.keyboard.isModifierActive('Alt') || openItem == true)
				item.sheet.render(true);
			else if (game.keyboard.isModifierActive('Control'))
				item.roll();
			else {
				let actionID = "";
				if (actionName) actionID = item.system.actions[actionName]?._id ?? (item.actions.get(actionName) ?? item.actions.getName(actionName))?.id ?? "";
				item.use({actionID, skipDialog: getSkipActionPrompt()});
			}
		}
	}
});

function renderSelect(selectActions, activeAction, remainingActions) {
	var options = templateOptions(),
		data = {}, hideUncharged = false,
		d, parsedList, showName, showType, useUUID, title;
	if (!selectActions)
		selectActions = remainingActions;
	if (typeof selectActions[0] == 'string') {
		parsedList = selectActions.map(o => o.match(argParse).slice(1));
		showName = parsedList.some(o => !!o[0] && o[0] != targetActor.name);
		showType = parsedList.some(o => !!o[3] && o[3] != targetType);
		selectActions = parsedList.map(o => {
			let t = {};
			let key = (o[0] ? o[0] + ': ' : '') + o[1] + (o[2] ? '; ' + o[2] : '') + (o[3] ? ' #' + o[3] : '');
			let val = (showName ? o[0] + ': ' : '') + o[1] + (o[2] ? '; ' + o[2] : '') + (showType ? ' #' + o[3] : ''); 
			return [key, val];
		});
		selectActions = Object.fromEntries(selectActions);
	}
	else if (selectActions[0] instanceof pf1.documents.item.ItemPF) {
		if (selectActions[0].type == 'spell') {
			selectActions.sort((a,b) =>  parseInt(a.system.level) - parseInt(b.system.level) || a.name.localeCompare(b.name, game.i18n.lang, {sensitivity: 'base'}));
			hideUncharged = true;
		}
		else selectActions.sort((a,b) => a.name.localeCompare(b.name, game.i18n.lang, {sensitivity: 'base'}));
		showName = selectActions.some((o,idx) => !(idx == 0) && o.actor != selectActions[idx-1].actor);
		showType = selectActions.some((o,idx) => !(idx == 0) && o.type != selectActions[idx-1].type);
		useUUID = selectActions.some(o => o.actor.isToken && showName);
		selectActions = selectActions.map(o => {
			var key = '', val = '';
			if (showName) {
				key += (useUUID ? '_' + (o.actor.isToken ? o.token.uuid : o.uuid) : o.actor.name) + ': ';
				val += o.actor.name + ': ';
			}
			key += o.name;
			val += o.name;
			if (showType) {
				key += ' #' + allTypes[targetType];
				val += ' #' + allTypes[targetType];
			}
			if ((o.type == 'spell' && o.charges > 0) || (o.type != 'spell' && o.charges - o.chargeCost >= 0))
				val += ' (' + o.charges + ')';
			return [key, val];
		});
		selectActions = Object.fromEntries(selectActions);
	}
	data = {selectActions, activeAction, hideUncharged};
	title = targetType === "All" ? game.i18n.translations.PF1.all :
		game.i18n.localize('TYPES.Item.' + targetType.toLowerCase());
	d = new Dialog({
		title,
		content: _templateCache['macroChain/useAction'](data, options),
		buttons: { 
			'ok': {
				icon: '<i class="fas fa-check"></i>',
				callback: (htm) => {
					var theRest = [htm[0].querySelector('select').value];
					if (remainingActions?.length > 0)
						theRest = theRest.concat(remainingActions);
					theRest = `_${targetActor.uuid}: ` + theRest.join(';');
					window.macroChain = window.macroChain?.concat([theRest]) ?? [theRest];
					game.macros.find(o => o.name == thisMacro)?.execute();
				}
			}
		}
	}, {width: 250});
	Hooks.once('renderDialog', (a,inp) => {
		function hideChargeless(e) {
			let checkChecked = e?.srcElement?.checked ?? e ?? true;
			let options = [...inp[0].querySelectorAll('option')];
			options.forEach(op => {if (!op.selected) op.style.display = !checkChecked || /\(\d+\)$/.test(op.innerText) ? '' : 'none'; });
		}
		inp[0].querySelector('input[name="toggleCharged"]').onchange = hideChargeless;
		hideChargeless(hideUncharged);
	});
	d.render(true);
	return true;
}

function rollAction(action, actor) {
	var foundAction,
		rollReg = new RegExp(`${c.commands.roll}:?\\s+`, 'i');
	action = action.replace(rollReg, '').toLocaleLowerCase();
	
	var skills = Object.entries(CONFIG.PF1.skills).map(s => [s[0], s[1].toLocaleLowerCase()]);
	skills.push(...Object.entries(actor.system.skills).flatMap(s => {
		let name = CONFIG.PF1.skills[s[0]]?.toLocaleLowerCase() ?? s[1].name;
		if (s[1].subSkills) {
			let subs = Object.entries(s[1].subSkills).map(p => [`${s[0]}.subSkills.${p[0]}`, p[1].name.toLocaleLowerCase()]);
			return subs.concat(subs.map(p => [p[0], `${name} (${p[1]})`]));
		}
		if (s[1].name) return [[s[0], s[1].name.toLocaleLowerCase()]];
		if (name.indexOf('(') > -1) return [[s[0], name.match(/\(([^)]+)\)/)[1]]];
		return [];
	}));
	foundAction = skills.find(s => s[1].localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0);
	if (foundAction) return actor.rollSkill(foundAction[0], { event: event, skipDialog: getSkipActionPrompt() });
	
	const abilities = Object.entries(CONFIG.PF1.abilities).map(a => [a[0], a[1].toLocaleLowerCase(), CONFIG.PF1.abilitiesShort[a[0]].toLocaleLowerCase()]);
	foundAction = abilities.find(s => {
		return (s[1].localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0) ||
		(s[2].localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0);
	});
	if (foundAction) return actor.rollAbility(foundAction[0], { event: event, skipDialog: getSkipActionPrompt() });
	
	const saves = Object.entries(CONFIG.PF1.savingThrows).map(s => [s[0], s[1].toLocaleLowerCase()]);
	foundAction = saves.find(s => s[1].localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0);
	if (foundAction) return actor.rollSavingThrow(foundAction[0], { event: event, skipDialog: getSkipActionPrompt() });
	
	if (game.i18n.translations.PF1.BAB.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0 ||
		game.i18n.translations.PF1.BABAbbr.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0)
		return actor.rollBAB({ event: event, skipDialog: getSkipActionPrompt() });

	if (game.i18n.translations.PF1.CMB.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0 ||
		game.i18n.translations.PF1.CMBAbbr.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0)
		return actor.rollCMB({ event: event, skipDialog: getSkipActionPrompt() });

	if (game.i18n.translations.PF1.Defenses.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0)
		return actor.rollDefenses();
	
	if (game.i18n.translations.PF1.Initiative.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0)
		return actor.rollInitiative({ createCombatants: true, rerollInitiative: game.user.isGM });
	
	if (game.i18n.translations.PF1.Melee.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0 ||
		game.i18n.translations.PF1.Ranged.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0)
		return actor.rollAttack({melee: game.i18n.translations.PF1.Melee.localeCompare(action, game.i18n.lang, {sensitivity: 'base'}) === 0});
		
	let searchReg = new RegExp('(?:(.+)\\s+)?(' + game.i18n.translations.PF1.CasterLevel + '|' + game.i18n.translations.PF1.Concentration + ')', 'i'),
		bookKey;
	[bookKey, foundAction] = action.match(searchReg)?.slice(1) ?? [];
	if (foundAction) {
		let reverseBooks = Object.fromEntries(Object.entries(actor.system.attributes.spells.spellbooks).filter(o => o[1].class != '' && o[1].class != '_hd').map(p => [p[1].class, p[0]]));
		if (!bookKey && Object.keys(reverseBooks).length) bookKey = reverseBooks[Object.keys(reverseBooks)[0]];
		else bookKey = reverseBooks[bookKey] || bookKey;
		if (bookKey) {
			if (game.i18n.translations.PF1.CasterLevel.localeCompare(foundAction, game.i18n.lang, {sensitivity: 'base'}) === 0)
				return actor.rollCL(bookKey);
			else if (game.i18n.translations.PF1.Concentration.localeCompare(foundAction, game.i18n.lang, {sensitivity: 'base'}) === 0)
				return actor.rollConcentration(bookKey);
		}
	}
		
	return false;	
}

function getSkipActionPrompt() {
	return (
		(game.settings.get("pf1", "skipActionDialogs") && !game.keyboard.isModifierActive("Shift")) ||
		(!game.settings.get("pf1", "skipActionDialogs") && game.keyboard.isModifierActive("Shift"))
	);
}

function templateOptions() {
	if(!_templateCache['macroChain/useAction']) {
		_templateCache['macroChain/useAction'] = Handlebars.compile(
`<form autocomplete="off">
	<div class="form-group">
		<label>{{localize "PF1.ActionPlural"}}: </label><div class="form-fields">
		<select name="action">
		{{#select activeAction}}
			{{#each selectActions}}
			<option value="{{@key}}">{{this}}</option>
			{{/each}}
		{{/select}}
		</select>
	</div></div>
	<div class="form-group">
		<label>${c.labels.hideChargeless}</label>
		<input type="checkbox" name="toggleCharged"{{#if hideUncharged}} checked{{/if}}>
</div></form>`
		);
	}
	return { allowProtoMethodsByDefault : true, allowProtoPropertiesByDefault: true };
}
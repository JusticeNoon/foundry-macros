/*-			DOCUMENTATION			-*/
// Syntax: [Operator] <Type> <Targets>
// [Set/Add/Remove/Clear] selected,target,Character Names
// ie Target template   /  Select myself  /  Target template,-me / clear Selection
// Type:
//		Target(s):			use the targetting system
//		Select(ion):		use the selection system
// Targets (Comma separated, "-" in front negates):
//		selected: 			Default if argument is provided. User who clicks the button
//		target(s/ed):		Targets of user who clicks the button
// 		Character Name:		All tokens on current scene linked to this actor. No commas in the name. Everything else is okay
//		template:			All character tokens within the last template of the user who clicks the button
//		me/myself:			Character the card containing the button came from
//		friendly/ies:		Tokens with disposition "friendly"
//		neutral(s):			Tokens with disposition "neutral" (GM only unless subtracting)
//		hostile(s):			Tokens with disposition "hostile" (GM only unless subtracting)
//		PC(s):				PC tokens
//		NPC(s):				NPC tokens (GM only unless subtracting)
// Operator
//		Set:				Default. overwrites all tokens to specified user
//		Add:				Adds the tokens to user instead
//		Remove:				Removes the tokens from user instead
//		Clear:				Sets type to blank. Ignores <Targets> If "Control" is held down during use, will overwrite operator to Clear
// When clicked from macrohotbar or not chat will set the current targets to Macro title using above syntax
// Supports label mode. @Macro[setTarget]{Target friendlies,-myself,-Lucky the Raven::Target everyone else}

/*-			COMMAND					-*/
try {
	if (typeof event === "undefined") event = new Event("none");
	if (event instanceof MessageEvent) var socketData = event.data ? JSON.parse(event.data.replace(/[^{([]+/,""))?.[0].result[0] : null;
	var inputText = window.macroChain?.pop() || (typeof args == "object" ? args.join(" ") : false) || event.target?.closest?.('button,a')?.textContent.trim(),
		chatMessage = socketData ? game.messages.get(socketData._id) : game.messages.get(event.target?.closest?.('.message')?.getAttribute('data-message-id')),
		argParse = /^(?:([sS]et|[aA]dd|[rR]emove|[cC]lear) )?([tT]argets?|[sS]elect(?:ion)?)(?: (.*))?$/,
		operator, systemType, targets,
		myself = event.args?.[1] ?? ChatMessage.getSpeakerActor(chatMessage?.speaker) ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		targetActors = [],
		excludedActors = [],
		tokenIds = [];
	if (event.target?.closest?.('button,a')?.dataset.extra)
		inputText = event.target.closest('button,a').dataset.extra;
	[operator, systemType, targets] = inputText.match(argParse).slice(1);
	
	if (!operator) operator = 'set';
	else operator = operator.toLowerCase();
	if (game.keyboard.isModifierActive("Control"))
		operator = 'clear';
	
	targets?.split(',').forEach(tar => {
		//actors here actually refers to tokens
		var accuActors;
		tar = tar.trim();
		if (tar.charAt(0) == '-') {
			accuActors = excludedActors;
			tar = tar.slice(1);
		}
		else
			accuActors = targetActors;
		switch(tar.toLowerCase()) {
			case 'selected':
				if (canvas.tokens.controlled.length == 0) ui.notifications.warn('No tokens selected');
				accuActors.push(...canvas.tokens.controlled);
				break;
			case 'template':
				let temp = event.args?.[5] ?? chatMessage?.flags.pf1?.metadata?.template ?? canvas.templates.placeables.filter(o => o.document.user.id == game.userId)?.pop()?.id;
				if (!temp) ui.notifications.warn('No template found');
				else {
					let tokens = canvas.tokens.placeables.filter(o => canvas.grid.getHighlightLayer('MeasuredTemplate.' + temp).geometry.containsPoint(o.center));
					accuActors.push(...tokens);
				}
				break;
			case 'target':
			case 'targets':
			case 'targeted':
				if (myself && game.users.find(o => o.character === myself) && game.actors.get(chatMessage?.speaker?.actor) === myself) {
					let targets = game.users.find(o => o.character === myself).targets;
					if (targets.size) {
						accuActors.push(...[...targets]);
						break;
					}
				}
				else if (chatMessage?.flags.pf1?.metadata?.targets.length)
					accuActors.push(...chatMessage.flags.pf1.metadata.targets.flatMap(o => canvas.tokens.get(o)));
				else
					accuActors.push(...[...game.user.targets]);
				break;
			case 'myself':
			case 'me':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.id == myself.id));
				break;
			case 'friendly':
			case 'friendlies':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == 1));
				break;
			case 'neutral':
			case 'neutrals':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == 0));
				break;
			case 'hostile':
			case 'hostiles':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == -1));
				break;
			case 'pcs':
			case 'pc':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.type == 'character'));
				break;
			case 'npcs':
			case 'npc':
				if (game.user.isGM || accuActors === excludedActors)
					accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.type == 'npc'));
				break;
			default:
				//Name or uuid search
				if (tar.indexOf('#') != 0) {
					let actName = game.actors.find(o => o.name == tar).name;
					accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.name == actName));
				}
				else {
					var [uScene, uToken, uActor] = tar.match(/^(?:Scene\.([^.]*))?(?:\.Token\.([^.]*))?(?:\.?Actor\.([^.]*))?$/).slice(1);
					if (uActor)
						accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.id == uActor));
					else if (uScene == canvas.id)
						accuActors.push(canvas.tokens.get(uToken));
				}
		}
	});
	excludedActors = excludedActors.map(o => o.id);
	targetActors = [...new Set(targetActors)].filter(o => o && !excludedActors.includes(o.id));
	excludedActors = [];
	
	//Written like this so it can bloat easier with the inevitable feature creep
	switch(systemType.toLowerCase()) {
		case 'target':
		case 'targets':
			systemType = 'target';
			tokenIds = targetActors.map(o => o.id);
			break;
		case 'select':
		case 'selection':
			systemType = 'select';
			targetActors = targetActors.filter(o => o.owner);
			break;
		default:
			throw 'Wrong Type. Use Select or Target';
	}
	
	switch(operator.toLowerCase()) {
		case 'set':
			if (systemType == 'target')
				game.user.updateTokenTargets(tokenIds);
			else {
				canvas.activeLayer.releaseAll();
				targetActors.forEach(o => {if (o.owner) o.control({releaseOthers: false})});
			}
			break;
		case 'add':
			if (systemType == 'target')
				game.user.updateTokenTargets(game.user.targets.ids.concat(tokenIds));
			else
				targetActors.forEach(o => {if (o.owner) o.control({releaseOthers: false})});
			break;
		case 'remove':
			if (systemType == 'target')
				game.user.updateTokenTargets(game.user.targets.ids.filter(p => !tokenIds.includes(p)));
			else
				targetActors.forEach(o => {o.release()});
			break;
		case 'clear':
			if (systemType == 'target')
				game.user.updateTokenTargets([]);
			else
				canvas.activeLayer.releaseAll();
			break;
	}
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
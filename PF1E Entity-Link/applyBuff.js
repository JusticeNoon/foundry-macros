/*-			DOCUMENTATION			-*/
// Syntax: <Operator> <Buff Name> [to/from/on <targets>] [as <Alt Name>] [at/by <Level>] [for <Length>] [in <distance> <unit>^<dimension>]
// Apply/Remove/Toggle/Delete/Activate Buff Name to/from/on selected,target,Character Names at Level in distance
// ie @Macro[applyBuff]{Apply Mage Armor to selected,-myself as Lightest Armor at @cl-1}
// Operator: 
//		Apply:			Create and Activate Buff
//		Remove: 		Delete and Deactivate Buff
//		Toggle: 		Applies if off, deactivates if on
//		Activate:		Turn on buff but only if present
//		Deactivate:		Create and turn off buff if it doesn't exist
//		Change:			Does nothing but update level
// Buff Name:
//		Will use the first Buff from the compendium. If compendiumPriority is specified below, will check those compendiums first, then pf1.commonbuffs.
//		Doesn't short circuit so every compendium provided might be checked
// Targets (Comma separated, "-" in front negates, same rules as Alt Name [see below]):
//		selected: 		Default if argument is provided. User who clicks the button
// 		Character Name:	No commas in the name. Everything else is okay. *Only updates linked actors (by design - convince me otherwise)*
//		target(s)(ed):	All targets of the user who clicks the button
//		template:		All character tokens within the last template of the user who clicks the button
//		me/myself:		Character the card containing the button came from
//		speaker:		Active speaker of the person who clicks the card
//		friendly/ies:	Tokens with disposition "friendly"
//		neutral(s):		Tokens with disposition "neutral"
//		hostile(s):		Tokens with disposition "hostile"
//		PC(s):			PC tokens
//		NPC(s):			NPC tokens
//		all:			All tokens on current scene
//		uuid:			Generally used internally for the time being. Prefix uuids with '#'
// Distance:
//		distance:		Distance in grid units
//		unit:			Unit is purely decorative
//		dimension:		Defaults 2d but will take into elevation with 3
//		This acts as a filter so it needs to be used in conjunction with a target selector above
// Alt Name:
//		Will search for both alt name and original. Quotes won't work here so use underscores(_) as spaces if you need to use any of the keywords (to,from,on,as,at)
// Level
//		number			Sets item level if supplied.
//      @cl-[1,2,3,s]	Accepts rollData strings. @cl-1, @cl-2, @cl-3, @cl-s are shorthands for @attributes.spells.spellbooks.X.cl.total
//		->				Will use the next inline roll results instead. Useful for pretty lines. If inside a "label", will use the value inside there
// Length
//		formula			Set formula to anything before unit. Use @@ anywhere to delay evaluation until buff calculation. Use ++/-- to set relative to existing duration
//		unit			[Turn(s), Round(s), Minute(s), Hour(s)]
//		unset			[ever, null, infinity, eternity, unset] can be used to unset duration on buffs already present.
// CONFIGURATION
// compendiumPriority	Will process compendia in order
// notifications		2/1/0/-1 :: All notifications/No unchanged/No warnings/No errors
// cleanupGMRequest		true/false :: After requesting GM to apply effects on un-owned tokens, whether to remove chat message from log
// Supports label mode. @Macro[applyBuff]{Apply Mage Armor to selected,-myself as Lightest Armor at ->::Activate Sparkle Barrier}[[@cl]]

/*-			CONFIGURATION			-*/
const c = {
	compendiumPriority: ["world.mybuffs", "pf-content.pf-buffs"],
	notifications: 2,
	cleanupGMRequest: true,
}

/*-			COMMAND					-*/
try {
	const keywords = ["to", "from", "on", "as", "at", "by", "within", "in", "for"];
	if (typeof event === "undefined") event = new Event("none");
	if (event instanceof MessageEvent) var socketData = event.data ? JSON.parse(event.data.replace(/[^{([]+/,""))?.[0].result[0] : null;
	var inputText = scope?.command?.replace(/_/g," ") || window.macroChain?.pop() || (typeof args == "object" ? args.join(" ") : false) || event.target?.closest?.('button,a')?.textContent.trim(),
		macroId = this.id,
		chatMessage = socketData ? game.messages.get(socketData._id) : game.messages.get(event.target?.closest?.('.message')?.getAttribute('data-message-id')),
		argParse = new RegExp('^([aA]pply|[rR]emove|[tT]oggle|[aA]ctivate|[dD]eactivate|[cC]hange) "?([^\\n]*?)"?( (?:' + keywords.join("|") + ')(?=([^"\\\\]*(\\\\.|"([^"\\\\]*\.)*[^"\\\\]*"))*[^"]*$) (?:.*))?$'),
		operator, buffName, modString,
		compCollNames = c.compendiumPriority.concat(['pf1.commonbuffs']),
		myself = event.args?.[1] ?? ChatMessage.getSpeakerActor(chatMessage?.speaker) ?? actor ?? game.user.character ?? canvas.tokens.controlled[0]?.actor,
		sourceItem = event.args?.[0] ?? chatMessage?.itemSource,
		targets,
		modList,
		targetActors = [],
		excludedActors = [],
		altName,
		levelOverride,
		durationFormula,
		durationUnit,
		distance, distUnit, distDimen,
		relativeLevel, relativeDuration;

	
	if (event.target?.closest?.('button,a')?.dataset.extra)
		inputText = event.target.closest('button,a').dataset.extra;
	[operator, buffName, modString] = inputText.match(argParse)?.filter(o => o).slice(1,4) ?? [];
	if (!buffName || !operator) throw notifier(`${game.i18n.localize('PF1.BuffName')}: "${buffName}"`, 2);
	if (modString || Object.keys(scope).length > 0) {
		let rollData = sourceItem?.getRollData?.() ?? myself?.getRollData?.() ?? {};
		let shorthand = {	'cl': rollData.cl || myself?.system.attributes.spells.spellbooks.primary.cl.total,
							'cl-1': myself?.system.attributes.spells.spellbooks.primary.cl.total,
							'cl-2': myself?.system.attributes.spells.spellbooks.secondary.cl.total,
							'cl-3': myself?.system.attributes.spells.spellbooks.tertiary.cl.total,
							'cl-s': myself?.system.attributes.spells.spellbooks.spelllike.cl.total};
		
		modList = modString?.split(new RegExp(" (" + keywords.join("|") + ") "))?.slice(1) ?? [];
		keywords.forEach(kw => {
			if (scope[kw]) {
				modList.push(kw, scope[kw].replace(/_/g, " "));
			}
		});
		for (var k = 0; k < modList.length; k += 2) {
			switch(modList[k]) {
				case 'to':
				case 'from':
				case 'on':
					targets = modList[k+1].replace(/_/g,' ');
					break;
				case 'as':
					altName = modList[k+1].replace(/_/g,' ');
					break;
				case 'by':	
				case 'at':
					if (modList[k+1].indexOf('->') > -1)
						modList[k+1] = event.target.nextElementSibling?.textContent.trim();
					levelOverride = modList[k+1];
					if (levelOverride.search(/[^\d]/) > -1) {
						levelOverride = RollPF.safeRollSync(levelOverride, mergeObject(rollData, shorthand)).total;
					}
					levelOverride = parseInt(levelOverride) || 0;
					if (modList[k] == 'by') relativeLevel = true;
					break;
				case 'within':
				case 'in':
					[distance, distUnit, distDimen]  = modList[k+1].match(/(\d+)(?:\s*([^\s^]*)\s*\^?(\d))?/)?.slice(1);
					distance = parseInt(distance) || 0;
					distDimen = parseInt(distDimen) || 2;
					break;
				case 'for':
					if (["ever", "null", "infinity", "eternity", "unset"].includes(modList[k+1])) {
						[durationFormula, durationUnit] = ["", ""];
						break;
					}
					[durationFormula, durationUnit] = modList[k+1].match(/(.+?) *(?:(Turn|Round|Minute|Hour)s?)/i)?.filter(o => o).slice(1) ?? [];
					if (durationFormula) {
						durationUnit = durationUnit.toLowerCase();
						if (durationFormula.includes('++') || durationFormula.includes('--')) {
							relativeDuration = true;
							durationFormula = durationFormula.replace(/\+\+|-(-)/g, '$1');
						}
						if (durationFormula.indexOf('@@') > -1)
							durationFormula = durationFormula.replace(/@@/g,'');
						else {
							durationFormula = RollPF.safeRollSync(durationFormula, mergeObject(rollData, shorthand)).total;
						}
					}
					break;
			}
		}
	}
	if (!targets) targets = 'selected';
	
	(targets ? targets.split(',') : ['selected']).forEach(tar => {
		var accuActors;
		tar = tar.trim();
		if (tar.charAt(0) == '-') {
			accuActors = excludedActors;
			tar = tar.slice(1);
		}
		else
			accuActors = targetActors;
		switch(tar.toLowerCase()) {
			case 'selected':
				if (canvas.tokens.controlled.length == 0) notifier('No tokens selected', 1);
				accuActors.push(...canvas.tokens.controlled.map(o => o.actor));
				break;
			case 'template':
				let temp = event.args?.[5] ?? chatMessage?.flags.pf1?.metadata?.template ?? canvas.templates.placeables.filter(o => o.document.user.id == game.userId)?.pop()?.id;
				if (!temp) notifier('No template found', 1);
				else {
					let tokens = canvas.tokens.placeables.filter(o => canvas.grid.getHighlightLayer('MeasuredTemplate.' + temp).geometry.containsPoint(o.center));
					accuActors.push(...tokens.map(o => o.actor));
				}
				break;
			case 'target':
			case 'targets':
			case 'targeted':
				if (myself && game.users.find(o => o.character === myself) && game.actors.get(chatMessage?.speaker?.actor) === myself) {
					let targets = game.users.find(o => o.character === myself).targets;
					if (targets.size) {
						accuActors.push(...[...targets].map(o => o.actor));
						break;
					}
				}
				if (chatMessage?.flags.pf1?.metadata?.targets.length)
					accuActors.push(...chatMessage.flags.pf1.metadata.targets.flatMap(o => fromUuidSync(o)?.actor));
				else
					accuActors.push(...[...game.user.targets].map(o => o.actor));
				break;
			case 'myself':
			case 'me':
			case 'self':
				accuActors.push(myself);
				break;
			case 'friendly':
			case 'friendlies':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == 1).map(o => o.actor));
				break;
			case 'neutral':
			case 'neutrals':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == 0).map(o => o.actor));
				break;
			case 'hostile':
			case 'hostiles':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.document.disposition == -1).map(o => o.actor));
				break;
			case 'pcs':
			case 'pc':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.type == 'character').map(o => o.actor));
				break;
			case 'npcs':
			case 'npc':
				accuActors.push(...canvas.tokens.placeables.filter(o => o.actor.type == 'npc').map(o => o.actor));
				break;
			case 'all':
				accuActors.push(...canvas.tokens.placeables.map(o => o.actor));
				break;
			case 'speaker':
				accuActors.push(pf1.documents.actor.ActorPF.getActiveActor());
				break;
			case 'remaining':
				tar = '#' + chatMessage?.flags.applyBuff?.remaining;
				if (tar.indexOf('undefined') > -1)
					break;
			default:
				//Name or uuid search
				if (tar.indexOf('#') != 0)
					accuActors.push(game.actors.find(o => o.name == tar));
				else {
					tar.split(',').forEach(uuid => {
						var [uScene, uToken, uActor] = uuid.match(/^#?(?:Scene\.([^.]*))?(?:\.Token\.([^.]*))?(?:\.?Actor\.([^.]*))?$/).slice(1);
						if (uActor && !uToken)
							accuActors.push(game.actors.get(uActor));
						else if (uScene == canvas.id)
							accuActors.push(canvas.tokens.get(uToken).actor);
					});
				}
		}
	});

	if (distance) {
		let myTok = myself.getActiveTokens(false, true)[0];
		targetActors = targetActors.filter(o => {
			let targetTok = o.token ?? o.getActiveTokens(false, true)[0],
				dist;
			if (targetTok) {
				dist = canvas.grid.measureDistance({x: myTok.x, y: myTok.y}, {x: targetTok.x, y: targetTok.y}, {gridSpaces: true});
				if (distDimen === 3)
					dist = canvas.grid.measureDistance({x: myTok.elevation * canvas.dimensions.distancePixels, y: 0}, {x: targetTok.elevation * canvas.dimensions.distancePixels, y: dist * canvas.dimensions.distancePixels}, {gridSpaces: true});
				if (dist <= distance) return true;
			}
			excludedActors.push(o);
			return false;
		});
	}
	excludedActors = excludedActors.flatMap(o => o ? o.id : []);
	targetActors = [...new Set(targetActors)].filter(o => o && !excludedActors.includes(o.id));
	excludedActors = [];

	let found;
	found = game.items.find(o => o.type == "buff" && o.name.localeCompare(buffName, undefined, { sensitivity: "base", ignorePunctuation: true }) == 0);
	if (found)
		return buffFound(found);
	else {
		found = pf1.utils.findInCompendia(buffName, {packs: compCollNames});
		if (found) {
			return found.pack.getDocument(found.index._id).then(doc => buffFound(doc));
		}
		else {
			/* Stub for FVTT !9984. Uses randomID as seed for indexCache invalidation */
			const packRefresh = compCollNames.map(o => game.packs.get(o)?.getIndex({fields: [randomID(8)]}));
			return Promise.all(packRefresh).then(() => {
				found = pf1.utils.findInCompendia(buffName, {packs: compCollNames});
				if (found)
					return found.pack.getDocument(found.index._id).then(doc => buffFound(doc));
				else {
					let foundOnChar;
					targetActors.find(a => {
						foundOnChar = foundOnChar || a.items.find(o => o.type == "buff" && o.name == buffName);
						return foundOnChar;
					});
					if (foundOnChar)
						return buffFound(foundOnChar);
				}
			});
		}
	}
}
catch (err) {
	console.log(err, "Whatever you did didn't work");
}
async function buffFound(buff) {
	const promises = [];
	if (buff) buff = buff.toObject();
	if (typeof levelOverride != 'undefined' && buff) buff.system.level = (relativeLevel ? parseInt(buff.system.level) : 0) + levelOverride + "";
	if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined' && buff) {
		if (relativeDuration) {
			let currentDur = buff.system.duration.value || 0;
			if (!(currentDur + '').match(/[^\d- ]|(?:.-)/)) currentDur = parseInt(currentDur);
			if (typeof durationFormula === 'string' || typeof currentDur === 'string') Object.assign(buff.system.duration, {value: currentDur + ' + ' + durationFormula, units: durationUnit});
			else Object.assign(buff.system.duration, {value: (currentDur + durationFormula) + '', units: durationUnit});
		} else Object.assign(buff.system.duration, {value: durationFormula + '', units: durationUnit});
	}
	if (typeof altName != 'undefined' && buff) buff.name = altName, buff.system.identifiedName = altName;
	targetActors.some(act => {
		if (act && act.isOwner) {
			let presentBuff = act.items.find(o => {return o.type == 'buff' && (o.name == buffName || o.name == altName);});

			if (presentBuff) {
				buff = mergeObject(presentBuff.toObject(), {}, {inplace: false});
				if (typeof levelOverride != 'undefined') buff.system.level = (relativeLevel ? parseInt(buff.system.level) : 0) + levelOverride + "";
				if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined') {
					if (relativeDuration) {
						let currentDur = buff.system.duration.value || 0;
						if (!(currentDur + '').match(/[^\d- ]|(?:.-)/)) currentDur = parseInt(currentDur);
						if (typeof durationFormula === 'string' || typeof currentDur === 'string') Object.assign(buff.system.duration, {value: currentDur + ' + ' + durationFormula, units: durationUnit});
						else Object.assign(buff.system.duration, {value: (currentDur + durationFormula) + '', units: durationUnit});
					} else Object.assign(buff.system.duration, {value: durationFormula + '', units: durationUnit});
				}
				if (typeof altName != 'undefined') buff.name = altName, buff.system.identifiedName = altName;
			}
			let updateObject = {'system.identifiedName': altName ?? buff.name, 'name': altName ?? buff.name};
			if (typeof levelOverride != 'undefined') updateObject['system.level'] = buff.system.level;
			if (typeof durationFormula != 'undefined' && typeof durationUnit != 'undefined') Object.assign(updateObject, {'system.duration.value': buff.system.duration.value, 'system.duration.units': durationUnit});
			if (!buff) return notifier(game.i18n.format('ERROR.lsNoItemFound', {item: buffName ?? altName}), 2), true;
			let active = false;
			switch(operator.toLowerCase()) {
				case 'apply':
					active = true;
					if (!buff.system.active)
						buff.system.active = true;
					
					if (presentBuff)
						promises.push(presentBuff.update(Object.assign({'system.active': true}, updateObject)));
					else
						promises.push(act.createEmbeddedDocuments("Item", [buff]));
					break;
				case 'remove':
					if (presentBuff)
						promises.push(presentBuff.delete());
					break;
				case 'toggle':
					if (presentBuff) {
						active = !getProperty(presentBuff.toObject(), 'system.active');
						promises.push(presentBuff.update(Object.assign({'system.active': active}, updateObject)));
					} else {
						buff.system.active = !buff.system.active;
						active = buff.system.active;
						
						promises.push(act.createEmbeddedDocuments("Item", [buff]));
					}
					break;
				case 'activate':
					active = true;
					if (presentBuff)
						promises.push(presentBuff.update(Object.assign({'system.active': true}, updateObject)));
					break;
				case 'deactivate':
					if (presentBuff)
						promises.push(presentBuff.update(Object.assign({'system.active': false}, updateObject)));
					else
						promises.push(act.createEmbeddedDocuments("Item", [buff]));
					break;
				case 'change':
					if (presentBuff)
						promises.push(presentBuff.update(updateObject));
						break;
				default:
					return notifier("That's not how you use this. See documentation.", 1);
			}
			if (presentBuff?.system.active && active) promises[promises.length - 1] = promises[promises.length - 1].then(p => presentBuff._updateTrackingEffect({system: {active: true}}));
		}
		else if (act)
			excludedActors.push(act);
	});
	var successStr = 'No tokens affected.',
		affectedActors = targetActors.length - excludedActors.length,
		hideMessage;
	if (affectedActors != 0 && buff)
		successStr = `${buff.name} was changed on ${affectedActors} token${(affectedActors > 1 ? 's' : '')}.`;
	if (excludedActors.length > 0) {
		if (c.notifications == 1 && excludedActors.every(a => a.permission === 2))
			hideMessage = false;
		else if (excludedActors.some(a => a.getActiveTokens().some(t => t.isVisible)))
			successStr += ' Requesting GM assistance for the rest.';
		else
			hideMessage = true;
		var remainingModList = modList.filter((o,p) => !["to", "on", "from"].includes(o) && !["to", "on", "from"].includes(modList[p-1])),
			excludedNames = excludedActors.map(o => (o.token ? o.token.name : o.name)).join(', '),
			gmButtonTitle = `${operator} ${buffName} to remaining ${remainingModList.join(' ')}`.trim(),
			enrichedButton = `<a class="entity-link content-link" draggable="true" data-type="Macro" data-id="${macroId}" data-uuid="Macro.${macroId}"><i class="fas fa-terminal"></i> ${gmButtonTitle}</a>`,
	flavorText = `I'm trying to<br>${operator} ${buffName} ${remainingModList.join(' ')} to: ${excludedNames}`,
			remainingUUID = excludedActors.map(o => (o.token ? o.token.uuid : o.uuid)).join(','),
			spoofedRoll = RollPF.safeRoll(excludedActors.length.toString()).toJSON(),
			messageData;
		spoofedRoll.formula = '#Affected:';
        spoofedRoll = Roll.fromJSON(JSON.stringify(spoofedRoll));
		messageData = ChatMessage.applyRollMode({
			sound: null,
			flavor: flavorText,
			speaker: ChatMessage.getSpeaker({actor: myself}),
			content: `${enrichedButton}`,
			type: CONST.CHAT_MESSAGE_TYPES.ROLL,
			roll: spoofedRoll,
			flags: mergeObject(chatMessage?.flags ?? {}, {applyBuff: {remaining: remainingUUID, cleanup: !!c.cleanupGMRequest}}, {inplace: false})
		}, "blindroll");
		const msg = await ChatMessage.create(messageData, {rollMode: "blindroll"});
		if (hideMessage) Hooks.once("renderChatMessage", (mess, htm, opt) => htm.css("display", "none"));
	}
	notifier(successStr.trim());
	if (chatMessage?.flags.applyBuff?.cleanup) chatMessage.delete();
	return Promise.all(promises);
}

function notifier(notif, level) {
	switch (level) {
		case 2:
			if (c.notifications > -1)
				ui.notifications.error(notif);
			return console.error(notif);
		case 1: 
			if (c.notifications > 0)
				return ui.notifications.warn(notif);
			else return console.warn(notif);
		default:
			if (c.notifications == 2 || (c.notifications == 1 && notif != 'No tokens affected.'))
				return ui.notifications.info(notif);
			else return notif;
	}
}
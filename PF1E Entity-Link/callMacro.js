/*-			DOCUMENTATION			-*/
//Duplicate and change this macro name to the button title
//set targetMacro below to target ie targetMacro = "applyBuff"
//set commandOverride below to ignore the title and use the command instead

/*-			CONFIGURATION			-*/
const targetMacro = "";
const commandOverride = "";

/*-			COMMAND					-*/
if (typeof shared !== "undefined") event.args = arguments;
window.macroChain = [commandOverride || this.name].concat(window.macroChain ?? []);
await game.macros.getName(targetMacro)?.execute({actor, token});


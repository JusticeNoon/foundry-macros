//Run this to apply updates. If you meet a minimum version requirement, you shouldn't lose any data.
//If you need to add your own code somewhere, create a header and it will be preserved
var updateList = ['applyBuff', 'useAction', 'setTarget', 'updateBook'],
	corsAnywhere = 'https://api.allorigins.win/raw?url=https://gitlab.com/JusticeNoon/foundry-macros/-/raw/master/PF1E%20Entity-Link/',
	url = corsAnywhere + updateList[0] + '.js',
	splitReg = /\/\*-\s*(.*?)\s*-\*\//gm,
	headerList = ['DOCUMENTATION', 'CONFIGURATION', 'COMMAND'];

let updated = await fetch(corsAnywhere + 'Utilities/Force Update All.js')
	.then(response => {
		if (!response.ok)
			throw new Error(`HTTP error! Status: ${response.status}`);
		return response.text();
	})
	.then(text => {
		if (text.length && text.indexOf("<") !== 0 && this.command !== text)
			return this.update({command: text});
		return false;
	});
if (updated) return updated.execute({actor, token});

updateList.forEach(o => {
	let url = corsAnywhere + o + '.js';
	fetch(url).then(response => {
		if (response.ok)
			return response.text();
		return Promise.reject(response);
	}).then(data => {
		var existing = game.macros.getName(o);
		if (!existing)
			return ui.notifications.warn(o + ': Not imported');
		else if (!data || !data.length || data.indexOf("<") === 0)
			throw ui.notifications.warn(o + ': Cannot retrieve'), {data};
		else if (existing.command == data)
			return 'Already updated';
			
		var existSplits = existing.command.split(splitReg);
		if (existSplits.length == 1) {
			let c = Dialog.confirm({
				title: 'Overwrite ' + o + '?',
				content: `<p> ${o} is so out of date it doesn't support partial updates. Pressing yes will replace it completely. Any user modications to ${o} will be lost.</p>`,
				yes: () => existing.update({command: data}),
				no: () => console.log('It\'s okay. Change is hard'),
				defaultYes: false
			});
		}
		else {
			if (existSplits.length % 2 == 1 && existSplits[0] == '')
				existSplits = toMap(existSplits.slice(1));
			else
				throw ui.notifications.error('Malformed target macro');
			
			var dataSplits = data.split(splitReg);
			if (dataSplits.length % 2 == 1 && dataSplits[0] == '')
				dataSplits = toMap(dataSplits.slice(1));
			else
				throw ui.notifications.error('Malformed source macro. Please @me on discord immediately');
			
			let formData = '';
			headerList.forEach(head => {
				if (!existSplits.has(head) && dataSplits.has(head)) {
					let tempExist = [...existSplits], insertPosition = [...dataSplits.keys()].indexOf(head);
					tempExist.splice(insertPosition, 0, [head, dataSplits.get(head)]);
					existSplits = new Map(tempExist);
					formData += '<h2>A  ' + head + ' section needs to be created.</h2><h2>It will be inserted when you press Apply.</h2>';
				}
				if (dataSplits.has(head) && dataSplits.get(head)?.trim().replace(/\r\n/g, '\n') != existSplits.get(head)?.trim().replace(/\r\n/g, '\n')) {
					if (head == 'CONFIGURATION' && dataSplits.get(head).match(/[^\s:]*:/g).length != existSplits.get(head).match(/[^\s:]*:/g).length)
						formData += '<h2> The configuration needs to be updated but cannot be done manually (missing keys). Selecting update will overwrite your configuration. Please take note before doing so.' + '</h2>';
					formData += `<label>${head} --- </label>`;
					formData += `<input type="radio" name="${head}" value="exist" ${(head == 'CONFIGURATION' ? 'checked' : '')}><label>Original</label>`;
					formData += `<input type="radio" name="${head}" value="data" ${(head != 'CONFIGURATION' ? 'checked' : '')}><label>Updated</label><br>`;
				}
			});
			if (formData == '')
				return 'No changes found. Headers must be different';
			else {
				const d = new Dialog({
					title: o + ' has differences',
					content: 	'<p>The following areas are different and need adjudication. \n' +
								'Choose which to keep. Areas not listed here will not be changed.</p><form>' +
								formData + '</form>',
					buttons: {
						apply: {
							icon: '<i class="fas fa-check"></i>',
							label: "Apply",
							callback: html => {
								let changes = Object.fromEntries((new FormData(html[0].querySelector('form'))).entries());
								let remake = '';
								existSplits.forEach((text, head) => {
									if (!changes[head] || changes[head] == 'exist')
										remake += '\n\n/*-\t\t\t' + head + '\t\t\t-*/\n' + text;
									else
										remake += '\n\n/*-\t\t\t' + head + '\t\t\t-*/\n' + dataSplits.get(head);
								});
								existing.update({command: remake.trim()});
							}
						},
						cancel: {
							icon: '<i class="fas fa-times"></i>',
							label: "Cancel",
							callback: () => {console.log('You can\'t cancel me. I quit.')}
						}
					},
					default: "apply"
				});
				d.render(true);
			}
		}
	}).catch(error => {
		console.warn('Something went wrong.', error);
	});
});

var toMap = function(r) {
	let myMap = new Map();
	for (var k = 0; k < r.length; k += 2)
		myMap.set(r[k], r[k+1].trim());
	return myMap;
}
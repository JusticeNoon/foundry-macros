const headers = ['/*-\t\t\tDOCUMENTATION\t\t\t-*/\n', '\n/*-\t\t\tCONFIGURATION\t\t\t-*/\n', '\n\n/*-\t\t\tCOMMAND\t\t\t\t\t-*/'];
const reg = /(?<=\/\*-\t\t\tCOMMAND\t\t\t\t\t-\*\/)\r?\n(.*game\.macros\.getName\(targetMacro\)\??.execute\([^)]*\);$)/gms;
const newMacro = `
if (typeof shared !== "undefined") event.args = arguments;
window.macroChain = [commandOverride || this.name].concat(window.macroChain ?? []);
await game.macros.getName(targetMacro)?.execute({actor, token});`

game.macros.forEach(m => {
	if (m !== this && reg.test(m.command)) {
		m.update({command: m.command.replace(reg, newMacro)});
	}
});

const affectedCalls = new Set();
const filterItem = (item) => {
	return item.scriptCalls?.filter(c => reg.test(c.data.value)) ?? []
};
const filterActor = (actor) => actor.items.contents.flatMap(i => filterItem(i));
const filterScene = (scene) => scene.tokens.contents.flatMap(t => t.isLinked ? [] : filterActor(t.actor));

game.actors.contents.flatMap(filterActor).forEach(s => affectedCalls.add(s));
game.items.contents.flatMap(filterItem).forEach(s => affectedCalls.add(s));
game.scenes.contents.flatMap(filterScene).forEach(s => affectedCalls.add(s));


for (const sc of affectedCalls) {
	await sc.update({value: sc.data.value.replace(reg, newMacro)});
}

ui.notifications.info(affectedCalls.size + " script calls updated");